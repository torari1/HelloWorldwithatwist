#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main() {
    char str[11] = "";  
    char alphabet[] = "abcdefghijklmnopqrstuvwxyz";
    int len_alphabet = strlen(alphabet);
    srand(time(NULL)); 

    while (strcmp(str, "helloworld") != 0) {  
        for (int i = 0; i < 10; i++) {
            str[i] = alphabet[rand() % len_alphabet];  
        }
        str[10] = '\0';  
        printf("%s\n", str);
    }
    printf("Success! The string is: %s\n", str);
    return 0;
}
